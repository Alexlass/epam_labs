﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using TicketReselling.DataAccess.Entities;

namespace TicketReselling.Presentation.Migrations
{
    [DbContext(typeof(TicketResellingContext))]
    [Migration("20170119141202_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.0-rtm-22752")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("TicketReselling.DataAccess.Entities.City", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("City");
                });

            modelBuilder.Entity("TicketReselling.DataAccess.Entities.Event", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Banner");

                    b.Property<DateTime>("Date");

                    b.Property<string>("Description");

                    b.Property<string>("Name");

                    b.Property<int>("VenueId");

                    b.HasKey("Id");

                    b.HasIndex("VenueId");

                    b.ToTable("Event");
                });

            modelBuilder.Entity("TicketReselling.DataAccess.Entities.Order", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("BuyerId");

                    b.Property<string>("Comment");

                    b.Property<string>("Status");

                    b.Property<int?>("TicketId");

                    b.Property<string>("TrackNumber");

                    b.HasKey("Id");

                    b.HasIndex("BuyerId");

                    b.HasIndex("TicketId");

                    b.ToTable("Order");
                });

            modelBuilder.Entity("TicketReselling.DataAccess.Entities.Role", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("Role");
                });

            modelBuilder.Entity("TicketReselling.DataAccess.Entities.Ticket", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("EventId");

                    b.Property<int>("Number");

                    b.Property<int>("Price");

                    b.Property<int>("SellerId");

                    b.Property<string>("SellerNotes");

                    b.Property<string>("Status");

                    b.HasKey("Id");

                    b.HasIndex("EventId");

                    b.HasIndex("SellerId");

                    b.ToTable("Ticket");
                });

            modelBuilder.Entity("TicketReselling.DataAccess.Entities.User", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<string>("FirstName");

                    b.Property<string>("LastName");

                    b.Property<string>("Localization");

                    b.Property<string>("Login");

                    b.Property<string>("Password");

                    b.Property<string>("PhoneNumber");

                    b.Property<int>("RoleId");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("User");
                });

            modelBuilder.Entity("TicketReselling.DataAccess.Entities.Venue", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address");

                    b.Property<int>("CityId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.HasIndex("CityId");

                    b.ToTable("Venue");
                });

            modelBuilder.Entity("TicketReselling.DataAccess.Entities.Event", b =>
                {
                    b.HasOne("TicketReselling.DataAccess.Entities.Venue", "Venue")
                        .WithMany("Events")
                        .HasForeignKey("VenueId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketReselling.DataAccess.Entities.Order", b =>
                {
                    b.HasOne("TicketReselling.DataAccess.Entities.User", "Buyer")
                        .WithMany("Orders")
                        .HasForeignKey("BuyerId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TicketReselling.DataAccess.Entities.Ticket", "Ticket")
                        .WithMany()
                        .HasForeignKey("TicketId");
                });

            modelBuilder.Entity("TicketReselling.DataAccess.Entities.Ticket", b =>
                {
                    b.HasOne("TicketReselling.DataAccess.Entities.Event", "Event")
                        .WithMany("Tickets")
                        .HasForeignKey("EventId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("TicketReselling.DataAccess.Entities.User", "Seller")
                        .WithMany("Tickets")
                        .HasForeignKey("SellerId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketReselling.DataAccess.Entities.User", b =>
                {
                    b.HasOne("TicketReselling.DataAccess.Entities.Role", "Role")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("TicketReselling.DataAccess.Entities.Venue", b =>
                {
                    b.HasOne("TicketReselling.DataAccess.Entities.City", "City")
                        .WithMany("Venues")
                        .HasForeignKey("CityId")
                        .OnDelete(DeleteBehavior.Cascade);
                });
        }
    }
}
