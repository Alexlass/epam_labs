﻿using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using AutoMapper;
using TicketReselling.Business.Entities;
using TicketReselling.Presentation.Models;
using TicketReselling.Business.Mappings;
using TicketReselling.DataAccess.Autofac;
using TicketReselling.Business.Autofac;
using Microsoft.Extensions.Configuration;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Identity;
using TicketReselling.Presentation.Identity;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Options;
using Microsoft.EntityFrameworkCore;
using TicketReselling.DataAccess.Entities;
using TicketReselling.DataAccess.Data;

namespace TicketReselling.Presentation
{
    public class Startup
    {
        public IContainer ApplicationContainer { get; private set; }

        public IConfigurationRoot Configuration { get; private set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            this.Configuration = builder.Build();

            if (env.IsDevelopment())
            { 
                builder.AddApplicationInsightsSettings(developerMode: true);
            }
            Configuration = builder.Build();
        }

       public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            var connection = @"Server=(localdb)\mssqllocaldb;Database=ResaleDB_<Aliaksandra_Kukushkina>;Trusted_Connection=True;";
            services.AddDbContext<TicketResellingContext>(options => options.UseSqlServer(connection));

            services.AddIdentity<UserModel, RoleModel>().AddDefaultTokenProviders();
            services.AddTransient<IUserStore<UserModel>, AppUserStore>();
           
            services.AddLocalization(opts => { opts.ResourcesPath = "Resources"; });

            services.AddMvc()
                .AddViewLocalization(
                    LanguageViewLocationExpanderFormat.Suffix,
                    opts => { opts.ResourcesPath = "Resources"; })
                .AddDataAnnotationsLocalization();

            services.Configure<RequestLocalizationOptions>(opts =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("en"),
                    new CultureInfo("ru"),
                    new CultureInfo("be"),
                };

                opts.DefaultRequestCulture = new RequestCulture(culture: "en", uiCulture: "en");
                // Formatting numbers, dates, etc.
                opts.SupportedCultures = supportedCultures;
                // UI strings that we have localized.
                opts.SupportedUICultures = supportedCultures;
            });

            //Настраиваем Autofac
            var builder = new ContainerBuilder();

            builder.RegisterModule(new DataAutofacConfigurations());
            builder.RegisterModule(new BusinessAutofacConfigurations());

            builder.RegisterType<RoleStore>().As<IRoleStore<RoleModel>>();
            builder.RegisterType<AppUserStore>().As<IUserStore<UserModel>>();

            builder.Populate(services);

            this.ApplicationContainer = builder.Build();
            
            return new AutofacServiceProvider(this.ApplicationContainer);
        }
        
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory, TicketResellingContext context)
        {
            var options = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();
            app.UseRequestLocalization(options.Value);

            app.UseIdentity();

            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationScheme = "Cookies",
                LoginPath = new PathString("/Account/Login"),
                AutomaticAuthenticate = true,
                AutomaticChallenge = true
            });
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //Настройка Automapper
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<CityModel, CityDTO>().PreserveReferences();
                cfg.CreateMap<CityDTO, CityModel>().PreserveReferences();

                cfg.CreateMap<EventModel, EventDTO>().PreserveReferences();
                cfg.CreateMap<EventDTO, EventModel>().PreserveReferences();

                cfg.CreateMap<EventDemostrationModel, EventDemostrationDTO>().PreserveReferences();
                cfg.CreateMap<EventDemostrationDTO, EventDemostrationModel>().PreserveReferences();

                cfg.CreateMap<TicketModel, TicketDTO>().PreserveReferences();
                cfg.CreateMap<TicketDTO, TicketModel>().PreserveReferences();

                cfg.CreateMap<UserModel, UserDTO>().PreserveReferences();
                cfg.CreateMap<UserDTO, UserModel>().PreserveReferences();

                cfg.CreateMap<VenueModel, VenueDTO>().PreserveReferences();
                cfg.CreateMap<VenueDTO, VenueModel>().PreserveReferences();

                cfg.CreateMap<OrderModel, OrderDTO>().PreserveReferences();
                cfg.CreateMap<OrderDTO, OrderModel>().PreserveReferences();

                cfg.CreateMap<RoleModel, RoleDTO>().PreserveReferences();
                cfg.CreateMap<RoleDTO, RoleModel>().PreserveReferences();

                cfg.AddProfile<BusinessMappingProfile>();
            });
            
            loggerFactory.AddConsole();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }            

            app.UseStaticFiles();

            DbInitializer.Initialize(app.ApplicationServices);
        }
    }
}
