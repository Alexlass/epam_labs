﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace TicketReselling.Presentation.Models
{
    public class UserModel : IdentityUser
    {        
        //public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Localization { get; set; }
        public string Address { get; set; }
        //public string PhoneNumber { get; set; }

        public RoleModel Role { get; set; }
        public int RoleId { get; set; }
        public List<TicketModel> Tickets { get; set; }
        public List<OrderModel> Orders { get; set; }
    }
}