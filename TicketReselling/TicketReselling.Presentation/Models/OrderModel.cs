﻿namespace TicketReselling.Presentation.Models
{
    public class OrderModel
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public string Comment { get; set; }
        public string TrackNumber { get; set; }

        public int BuyerId { get; set; }
        public UserModel Buyer { get; set; }
        //public int TicketId { get; set; }
        public TicketModel Ticket { get; set; }
    }
}
