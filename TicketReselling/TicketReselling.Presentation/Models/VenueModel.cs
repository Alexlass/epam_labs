﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TicketReselling.Presentation.Models
{
     public class VenueModel
     {
        public int Id { get; set; }
        [Required(ErrorMessage = "The field must be filled")]
        public string Name { get; set; }
        [Required(ErrorMessage = "The field must be filled")]
        public string Address { get; set; }

        [Required(ErrorMessage = "The field must be filled")]
        public int CityId { get; set; }
        public CityModel City { get; set; }
        public List<EventModel> Events { get; set; }
    }
}
