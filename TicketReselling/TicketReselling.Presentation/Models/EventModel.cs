﻿using System.ComponentModel.DataAnnotations;
using System;
using System.Collections.Generic;

namespace TicketReselling.Presentation.Models
{
    public class EventModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Banner { get; set; }
        public string Description { get; set; }

        public int VenueId { get; set; }
        public VenueModel Venue { get; set; } 
        public List<TicketModel> Tickets { get; set; }
    }

    public class EventDemostrationModel
    {
        public int Id { get; set; }
        public string Banner { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string CityName { get; set; }
        public string VenueName { get; set; }
        public string Description { get; set; }
    }
}