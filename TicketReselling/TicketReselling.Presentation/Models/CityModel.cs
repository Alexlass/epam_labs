﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TicketReselling.Presentation.Models
{
    public class CityModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "The field must be filled")]
        public string Name { get; set; }

        public List<VenueModel> Venues { get; set; }
    }
}