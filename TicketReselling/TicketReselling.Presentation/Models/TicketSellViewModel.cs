﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TicketReselling.Presentation.Models
{
    public class TicketSellViewModel
    {
        public int TicketId { get; set; }
        public string SellerNotes { get; set; }
    }
}
