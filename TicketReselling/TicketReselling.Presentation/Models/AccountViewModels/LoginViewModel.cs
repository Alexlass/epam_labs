﻿using System.ComponentModel.DataAnnotations;

namespace TicketReselling.Presentation.Models.AccountViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "field must be filled")]
        [Display(Name = "Login")]
        public string Login { get; set; }

        [Required(ErrorMessage = "field must be filled")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}