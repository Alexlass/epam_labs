﻿using System.Collections.Generic;

namespace TicketReselling.Presentation.Models
{
    public class TicketModel
    {
        public int Id { get; set; }   
        public int Number { get; set; }
        public int Price { get; set; }  
        public string SellerNotes { get; set; }
        public string Status { get; set; }
        
        public int EventId { get; set; }
        public EventModel Event { get; set; }
        public int SellerId { get; set; }
        public UserModel Seller { get; set; }
    }
}
