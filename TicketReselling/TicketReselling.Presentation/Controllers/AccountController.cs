﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TicketReselling.Business.Interfaces;
using TicketReselling.Presentation.Models.AccountViewModels;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using TicketReselling.Presentation.Models;
using Microsoft.Extensions.Localization;

namespace TicketReselling.Presentation.Controllers
{
    [AllowAnonymous]
    public class AccountController : Controller
    {
        private IUserService userService;
        private IRoleService roleService;
        private readonly UserManager<UserModel> userManager;
        private readonly SignInManager<UserModel> signInManager;
        private readonly IStringLocalizer<AccountController> _localizer;

        public AccountController(IUserService userService, UserManager<UserModel> userManager, 
            SignInManager<UserModel> signInManager, IStringLocalizer<AccountController> localizer, IRoleService roleService)
        {
            this.roleService = roleService;
            this.userService = userService;
            this.userManager = userManager;
            this.signInManager = signInManager;
            _localizer = localizer;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                string hashPass = userService.GetMD5Hash(model.Password);
                 //var result = await signInManager.PasswordSignInAsync(model.Login, hashPass, false, lockoutOnFailure: false);
                // if (result.Succeeded)
                {
                    var user = userManager.FindByNameAsync(model.Login).Result;
                    if (user != null && user.Password == hashPass)
                    {
                        await Authenticate(user.Login); // аутентификация
                        return RedirectToAction("Index", "Home");
                    }
                }
                ModelState.AddModelError("", _localizer["WrongPassword"]);
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = userService.GetUserByLogin(model.Login);
                if (user == null)
                {                    
                    userService.Create(model.Login, model.Password);

                    await Authenticate(model.Login);
                    return RedirectToAction("UpdateUserInfo", "Home");
                }
                else
                    ModelState.AddModelError("", _localizer["WrongPassword"]);
            }
            return View(model);
        }

        private async Task Authenticate(string login)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, login),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, userService.GetRoleNameByLogin(login))
            };
            // создаем объект ClaimsIdentity
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType,
            ClaimsIdentity.DefaultRoleClaimType);
            // установка аутентификационных куки            
            await HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }

        public async Task<IActionResult> Logout()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");
            return RedirectToAction("Login", "Account");
        }
    }
}