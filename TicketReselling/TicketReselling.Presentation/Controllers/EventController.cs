﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TicketReselling.Business.Interfaces;
using Microsoft.Extensions.Localization;
using TicketReselling.Presentation.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Hosting;

namespace TicketReselling.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class EventController : Controller
    {
        IEventService eventService;
        private IHostingEnvironment _environment;
        private readonly IStringLocalizer<EventController> _localizer;

        public EventController(IEventService eventService, IStringLocalizer<EventController> localizer, IHostingEnvironment environment)
        {
            this.eventService = eventService;
            _environment = environment;
            _localizer = localizer;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var events = eventService.GetAllEvents();
            return View(Mapper.Map<List<EventModel>>(events));
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(EventModel model)
        {
            if (ModelState.IsValid)
            {
                var item = eventService.GetAllEvents().FirstOrDefault(e => e.Name == model.Name && e.VenueId == model.VenueId && e.Date == model.Date);
                if (item == null)
                {
                    eventService.Create(model.Name, model.Date, model.Banner, model.Description, model.VenueId);
                    int eventId = eventService.GetAllEvents().FirstOrDefault(e => e.Name == model.Name && e.VenueId == model.VenueId && e.Date == model.Date).Id;

                    return RedirectToAction("ImgUpload", "Event", new { eventId = eventId });
                }
                else ModelState.AddModelError("", _localizer["Event already exists"]);
            }
            return View();
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var item = eventService.GetEvent(id);
            return View(Mapper.Map<EventModel>(item));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(EventModel model)
        {
            if (ModelState.IsValid)
            {
                var item = eventService.GetEvent(model.Id);
                if (item != null)
                {
                    eventService.Update(model.Id, model.Name, model.Date, model.Banner, model.Description, model.VenueId);
                    return RedirectToAction("ImgUpload", "Event");
                }
                else ModelState.AddModelError("", _localizer["Action can't be performed"]);
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var item = eventService.GetEvent(id);
            return View(Mapper.Map<EventModel>(item));
        }

        [HttpPost]
        public IActionResult DeleteConfirmation(int id)
        {
            eventService.Delete(id);
            return RedirectToAction("Index", "Event");
        }

        [HttpGet]
        public IActionResult ImgUpload(int eventId)
        {
            ViewBag.EventId = eventId;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ImgUpload(IFormFile file, int eventId)
        {
            if (file != null)
            {
                var item = eventService.GetEvent(eventId);
                item.Banner = "\\images\\EventImages\\" + item.Id + "_" + file.FileName;
                eventService.Update(item);
                
                using (var fileStream = new FileStream(_environment.WebRootPath + item.Banner, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }
            }
            return RedirectToAction("Index", "Event");
        }

    }
}
