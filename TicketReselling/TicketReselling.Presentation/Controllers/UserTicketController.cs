﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TicketReselling.Business.Interfaces;
using TicketReselling.Presentation.Models;
using AutoMapper;
using System.Linq;

namespace TicketReselling.Presentation.Controllers
{
    [Authorize]
    public class UserTicketController : Controller
    {
        IOrderService orderService;
        ITicketService ticketService;
        IUserService userService;

        public UserTicketController(IOrderService orderService, ITicketService ticketService, IUserService userService)
        {
            this.orderService = orderService;
            this.ticketService = ticketService;
            this.userService = userService;
        }

        [HttpGet]
        public IActionResult ShowUserTickets()
        {
            return View(Mapper.Map<List<TicketModel>>(ticketService.GetUserTickets(User.Identity.Name)));
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(TicketModel model)
        {
            if (ModelState.IsValid)
            {
                var user = userService.GetUserByLogin(User.Identity.Name);
                ticketService.Create(model.EventId, user.Id, model.Price, model.Number);
                return RedirectToAction("ShowUserTickets", "UserTicket");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Detail(int id)
        {
            var ticket = ticketService.GetTicket(id);
            return View(Mapper.Map<TicketModel>(ticket));
        }

        [HttpGet]
        public IActionResult Confirm(int id)
        {
            var model = new TicketSellViewModel();
            model.TicketId = id;
            model.SellerNotes = "";
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Confirm(TicketSellViewModel model)
        {
            if (ModelState.IsValid)
            {
                var order = orderService.GetAllOrders().FirstOrDefault(o => o.Ticket.Id == model.TicketId);
                orderService.Confirm(order);
                ticketService.Confirm(order.Ticket.Id);
                return RedirectToAction("ShowUserTickets", "UserTicket");
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Reject(int id)
        {
            var model = new TicketSellViewModel();
            model.TicketId = id;
            model.SellerNotes = "";
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Reject(TicketSellViewModel model)
        {
            if (ModelState.IsValid)
            {
                var order = orderService.GetAllOrders().FirstOrDefault(e => e.Ticket.Id == model.TicketId);
                orderService.Reject(order);
                ticketService.Reject(order.Ticket.Id);
                return RedirectToAction("ShowUserTickets", "UserTicket");
            }
            return View(model);
        }

    }
}