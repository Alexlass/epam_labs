﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TicketReselling.Business.Interfaces;
using AutoMapper;
using TicketReselling.Presentation.Models;
using System.Linq;

namespace TicketReselling.Presentation.Controllers
{
    public class TicketController : Controller
    {
        IOrderService orderService;
        ITicketService ticketService;

        public TicketController(IOrderService orderService, ITicketService ticketService)
        {
            this.orderService = orderService;
            this.ticketService = ticketService;
        }

        [HttpGet]
        public IActionResult ShowTickets(int id)
        {
            var tickets = Mapper.Map<List<TicketModel>>(ticketService.GetTicketsByEvent(id).Where(t => t.Status == "Selling"));
            return View(tickets);
        }
    }
}