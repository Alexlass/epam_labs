﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TicketReselling.Business.Interfaces;
using Microsoft.Extensions.Localization;
using TicketReselling.Presentation.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TicketReselling.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class VenueController : Controller
    {
        IVenueService venueService;
        private readonly IStringLocalizer<VenueController> _localizer;

        public VenueController(IVenueService venueService, IStringLocalizer<VenueController> localizer)
        {
            this.venueService = venueService;
            _localizer = localizer;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var venues = venueService.GetAllVenues();
            return View(Mapper.Map<List<VenueModel>>(venues));
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(VenueModel model)
        {
            if (ModelState.IsValid)
            {
                var venue = venueService.GetAllVenues().FirstOrDefault(c => c.Name == model.Name && c.CityId == model.CityId);
                if (venue == null)
                {
                    venueService.Create(model.Name, model.Address, model.CityId);
                    return RedirectToAction("Index", "Venue");
                }
                else ModelState.AddModelError("", _localizer["Venue already exists"]);
            }
            return View();
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var venue = venueService.GetVenue(id);
            return View(Mapper.Map<VenueModel>(venue));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(VenueModel model)
        {
            if (ModelState.IsValid)
            {
                var venue = venueService.GetVenue(model.Id);
                if (venue != null)
                {
                    venueService.Update(model.Id, model.Name, model.Address, model.CityId);
                    return RedirectToAction("Index", "Venue");
                }
                else ModelState.AddModelError("", _localizer["Action can't be performed"]);
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var venue = venueService.GetVenue(id);
            return View(Mapper.Map<VenueModel>(venue));
        }

        [HttpPost]
        public IActionResult DeleteConfirmation(int id)
        {
            venueService.Delete(id);
            return RedirectToAction("Index", "Venue");
        }
    }
}
