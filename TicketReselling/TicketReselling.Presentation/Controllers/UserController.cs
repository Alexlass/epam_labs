﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TicketReselling.Business.Interfaces;
using Microsoft.Extensions.Localization;
using TicketReselling.Presentation.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TicketReselling.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class UserController : Controller
    {
        IUserService userService;
        private readonly IStringLocalizer<UserController> _localizer;

        public UserController(IUserService userService, IStringLocalizer<UserController> localizer)
        {
            this.userService = userService;
            _localizer = localizer;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var users = userService.GetAllUsers();
            return View(Mapper.Map<List<UserModel>>(users));
        }

        [HttpGet]
        public IActionResult ChangeRights(int id)
        {
            var user = userService.GetUser(id);
            if (user != null)
            {
                user.RoleId = user.RoleId == 1 ? 2 : 1;
                userService.Update(user);
                return RedirectToAction("Index", "User");
            }
            else ModelState.AddModelError("", _localizer["Action can't be performed"]);

            return RedirectToAction("Index", "User");
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var user = userService.GetUser(id);
            return View(Mapper.Map<UserModel>(user));
        }

        [HttpPost]
        public IActionResult DeleteConfirmation(int id)
        {
            userService.Delete(id);
            return RedirectToAction("Index", "User");
        }
    }
}
