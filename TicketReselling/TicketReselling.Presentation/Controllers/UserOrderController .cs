﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TicketReselling.Business.Interfaces;
using TicketReselling.Presentation.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using System.Linq;

namespace TicketReselling.Presentation.Controllers
{
    public class UserOrderController : Controller
    {
        IOrderService orderService;
        ITicketService ticketService;
        IUserService userService;

        public UserOrderController(IOrderService orderService, ITicketService ticketService, IUserService userService)
        {
            this.orderService = orderService;
            this.ticketService = ticketService;
            this.userService = userService;
        }

        public IActionResult ShowUserOrders()
        {
            return View(Mapper.Map<List<OrderModel>>(orderService.GetUserOrders(User.Identity.Name)));
        }

        [HttpGet]
        public IActionResult Order(int id)
        {
            if(User.Identity.IsAuthenticated)
            {
                var ticket = ticketService.GetTicket(id);
                return View(Mapper.Map<TicketModel>(ticket));
            }
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public IActionResult OrderConfirmation(int id)
        {
            var buyer = userService.GetUserByLogin(User.Identity.Name);
            orderService.Create(buyer.Id, id);
            orderService.NotifyTheSeller(id);

            return RedirectToAction("Index", "Home");
        }
    }
}
