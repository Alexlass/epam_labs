﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using TicketReselling.Business.Interfaces;
using AutoMapper;
using TicketReselling.Presentation.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Localization;
using System.Threading.Tasks;

namespace TicketReselling.Presentation.Controllers
{
    public class HomeController : Controller
    {
        IEventService eventService;
        IUserService userService;
        //IMapper mapper;
        private readonly IStringLocalizer<HomeController> _localizer;

        public HomeController(IEventService eventService, IUserService userService, IStringLocalizer<HomeController> localizer)
        {
            this.eventService = eventService;
            this.userService = userService;
            _localizer = localizer;
        }

        public IActionResult Index()
        {
            var events = eventService.DisplayAllEvents();
            return View(Mapper.Map<List<EventDemostrationModel>>(events));
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = _localizer["This is a resource for reselling of your tickets."];

            return View();
        }

        [Authorize]
        public IActionResult UserInfo()
        {
            var user = userService.GetUserByLogin(User.Identity.Name);
            return View(Mapper.Map<UserModel>(user));
        }

        [Authorize]
        [HttpGet]
        public IActionResult UpdateUserInfo()
        {
            var user = userService.GetUserByLogin(User.Identity.Name);
            return View(Mapper.Map<UserModel>(user));
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult UpdateUserInfo(UserModel model)
        {
            if (ModelState.IsValid)
            {
                var user = userService.GetUserByLogin(model.Login);
                if (user != null)
                {
                    userService.Update(Int32.Parse(model.Id), model.Login, model.Password, model.FirstName, model.LastName, model.Address, model.PhoneNumber, model.Localization, 2);
                    return RedirectToAction("Index", "Home");
                }
                else ModelState.AddModelError("", _localizer["Action can't be performed"]);
            }
            return View(model);            
        }

        [HttpPost]
        public IActionResult SetLanguage(string culture, string returnUrl)
        {
            Response.Cookies.Append(
                CookieRequestCultureProvider.DefaultCookieName,
                CookieRequestCultureProvider.MakeCookieValue(new RequestCulture(culture)),
                new CookieOptions { Expires = DateTimeOffset.UtcNow.AddYears(1) }
            );

            return LocalRedirect(returnUrl);
        }

        public IActionResult Error()
        {
            return View();
        }
        
    }
}