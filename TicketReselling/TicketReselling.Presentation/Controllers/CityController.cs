﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using TicketReselling.Business.Interfaces;
using Microsoft.Extensions.Localization;
using AutoMapper;
using TicketReselling.Presentation.Models;

namespace TicketReselling.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CityController : Controller
    {
        ICityService cityService;
        private readonly IStringLocalizer<CityController> _localizer;

        public CityController(ICityService cityService, IStringLocalizer<CityController> localizer)
        {
            this.cityService = cityService;
            _localizer = localizer;
        }

        [HttpGet]
        public IActionResult Index()
        {
            var cities = cityService.GetAllCities();
            return View(Mapper.Map<List<CityModel>>(cities));
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CityModel model)
        {            
            if (ModelState.IsValid)
            {
                var city = cityService.GetAllCities().FirstOrDefault(c => c.Name == model.Name);
                if (city == null)
                {
                    cityService.Create(model.Name);
                    return RedirectToAction("Index", "City");
                }
                else ModelState.AddModelError("", _localizer["City already exists"]);
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            var city = cityService.GetCity(id);
            return View(Mapper.Map<CityModel>(city));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(CityModel model)
        {
            if (ModelState.IsValid)
            {
                var city = cityService.GetCity(model.Id);
                if (city != null)
                {
                    cityService.Update(model.Id, model.Name);
                    return RedirectToAction("Index", "City");
                }
                else ModelState.AddModelError("", _localizer["Action can't be performed"]);
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Delete(int id)
        {
            var city = cityService.GetCity(id);
            return View(Mapper.Map<CityModel>(city));
        }

        [HttpPost]
        public IActionResult DeleteConfirmation(int id)
        {
            cityService.Delete(id);
            return RedirectToAction("Index", "City");
        }
    }
}
