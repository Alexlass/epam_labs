﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketReselling.Business.Entities;
using TicketReselling.DataAccess.Entities;

namespace TicketReselling.Business.Mappings
{
    public class BusinessMappingProfile : Profile
    {
        public BusinessMappingProfile()
        {
            CreateMap<CityDTO, City>().PreserveReferences();
            CreateMap<City, CityDTO>().PreserveReferences();

            CreateMap<EventDTO, Event>().PreserveReferences();
            CreateMap<Event, EventDTO>().PreserveReferences();

            CreateMap<OrderDTO, Order>().PreserveReferences();
            CreateMap<Order, OrderDTO>().PreserveReferences();

            CreateMap<TicketDTO, Ticket>().PreserveReferences();
            CreateMap<Ticket, TicketDTO>().PreserveReferences();

            CreateMap<UserDTO, User>().PreserveReferences();
            CreateMap<User, UserDTO>().PreserveReferences();

            CreateMap<VenueDTO, Venue>().PreserveReferences();
            CreateMap<Venue, VenueDTO>().PreserveReferences();

            CreateMap<RoleDTO, Role>().PreserveReferences();
            CreateMap<Role, RoleDTO>().PreserveReferences();

        }
    }
}
