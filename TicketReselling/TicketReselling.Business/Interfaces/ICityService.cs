﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketReselling.Business.Entities;

namespace TicketReselling.Business.Interfaces
{
    public interface ICityService
    {
        IEnumerable<CityDTO> GetAllCities();

        CityDTO GetCity(int id);

        void Create(string name);

        void Update(int id, string newName);

        void Delete(int id);
    }
}
