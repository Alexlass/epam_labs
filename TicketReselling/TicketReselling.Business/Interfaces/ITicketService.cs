﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketReselling.Business.Entities;

namespace TicketReselling.Business.Interfaces
{
    public interface ITicketService
    {
        IEnumerable<TicketDTO> GetAllTickets();

        TicketDTO GetTicket(int id);

        IEnumerable<TicketDTO> GetTicketsByEvent(int eventId);

        IEnumerable<TicketDTO> GetUserTickets(string login);

        IEnumerable<TicketDTO> GetUserSellingTickets(string login);

        IEnumerable<TicketDTO> GetUserWaitingConfirmationTickets(string login);

        IEnumerable<TicketDTO> GetUserSoldTickets(string login);

        void Create(int eventId, int sellerId, int price, int number);

        void Reject(int id);

        void Confirm(int id);

        void Delete(int id);
    }
}
