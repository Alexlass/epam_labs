﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketReselling.Business.Entities;

namespace TicketReselling.Business.Interfaces
{
    public interface IRoleService
    {
        IEnumerable<RoleDTO> GetAllRoles();

        RoleDTO GetRole(int id);

        RoleDTO GetRoleByName(string name);

        string GetRoleNameByUserName(string name);
    }
}
