﻿using System;
using System.Collections.Generic;
using TicketReselling.Business.Entities;

namespace TicketReselling.Business.Interfaces
{
    public interface IEventService
    {
        IEnumerable<EventDTO> GetAllEvents();

        EventDTO GetEvent(int id);

        IEnumerable<EventDemostrationDTO> DisplayAllEvents();

        void Create(string name, DateTime date, string banner, string description, int venueId);

        void Update(int id, string name, DateTime date, string banner, string description, int venueId);

        void Update(EventDTO eventDTO);

        void Delete(int id);
    }
}
