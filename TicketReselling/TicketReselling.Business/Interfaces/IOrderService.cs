﻿using System;
using System.Collections.Generic;
using TicketReselling.Business.Entities;

namespace TicketReselling.Business.Interfaces
{
    public interface IOrderService
    {
        IEnumerable<OrderDTO> GetAllOrders();

        OrderDTO GetOrder(int id);

        IEnumerable<OrderDTO> GetUserOrders(string login);

        IEnumerable<OrderDTO> GetUserConfirmedOrders(string login);

        IEnumerable<OrderDTO> GetUserWaitingConfirmationOrders(string login);

        IEnumerable<OrderDTO> GetUserRejectedOrders(string login);

        void Create(int buyerId, int ticketId);

        void NotifyTheSeller(int ticketId);

        void Reject(OrderDTO order);

        void Confirm(OrderDTO order);

        void Delete(int id);
    }
}
