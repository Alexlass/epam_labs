﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketReselling.Business.Entities;

namespace TicketReselling.Business.Interfaces
{
    public interface IUserService
    {
        IEnumerable<UserDTO> GetAllUsers();

        UserDTO GetUser(int id);

        UserDTO GetUserByLoginAndPassword(string log, string pass);

        UserDTO GetUserByNormalizedLogin(string log);

        UserDTO GetUserByLogin(string log);

        string GetMD5Hash(string pass);

        void Create(string login, string pass, string fname, string lname, string address, string phone, string localization, int roleId);

        void Create(string login, string pass);

        void Update(int id, string login, string pass, string fname, string lname, string address, string phone, string localization, int roleId);

        void Update(UserDTO user);

        void Delete(int id);

        string GetRoleNameByLogin(string log);
    }
}
