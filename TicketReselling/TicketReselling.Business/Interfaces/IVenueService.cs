﻿using System;
using System.Collections.Generic;
using TicketReselling.Business.Entities;

namespace TicketReselling.Business.Interfaces
{
   public interface IVenueService
    {
        IEnumerable<VenueDTO> GetAllVenues();

        IEnumerable<VenueDTO> GetVenuesByCityId(int cityId);

        VenueDTO GetVenue(int id);

        void Create(string name, string address, int cityId);

        void Update(int id, string newName, string address, int cityId);

        void Delete(int id);
    }
}
