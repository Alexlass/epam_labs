﻿using System;
using System.Collections.Generic;

namespace TicketReselling.Business.Entities
{
    public class EventDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Banner { get; set; }
        public string Description { get; set; }

        public int VenueId { get; set; }
        public VenueDTO Venue { get; set; }
        public List<TicketDTO> Tickets { get; set; }
    }


    public class EventDemostrationDTO
    {
        public int Id { get; set; }
        public string Banner { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string CityName { get; set; }
        public string VenueName { get; set; }
        public string Description { get; set; }
    }
}
