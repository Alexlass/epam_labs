﻿using System.Collections.Generic;

namespace TicketReselling.Business.Entities
{
    public class RoleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<UserDTO> Users { get; set; }
        public RoleDTO()
        {
            Users = new List<UserDTO>();
        }
    }
}
