﻿using System;
using System.Collections.Generic;

namespace TicketReselling.Business.Entities
{
    public class VenueDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }

        public int CityId { get; set; }
        public CityDTO City { get; set; }
        public List<EventDTO> Events { get; set; }
    }

}
