﻿namespace TicketReselling.Business.Entities
{
    public class OrderDTO
    {
        public int Id { get; set; }
        public string Status { get; set; }
        public string Comment { get; set; }
        public string TrackNumber { get; set; }

        //public int TicketId { get; set; }
        public TicketDTO Ticket { get; set; }
        public int BuyerId { get; set; }
        public UserDTO Buyer { get; set; }        
    }

}
