﻿
using System;
using System.Collections.Generic;

namespace TicketReselling.Business.Entities
{
    public class CityDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public List<VenueDTO> Venues { get; set; }
    }
}
