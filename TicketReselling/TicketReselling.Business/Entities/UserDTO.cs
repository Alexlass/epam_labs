﻿
using System.Collections.Generic;

namespace TicketReselling.Business.Entities
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Localization { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        public RoleDTO Role { get; set; }
        public int RoleId { get; set; }
        public List<TicketDTO> Tickets { get; set; }
        public List<OrderDTO> Orders { get; set; }
    }

}
