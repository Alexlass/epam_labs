﻿using System.Collections.Generic;

namespace TicketReselling.Business.Entities
{
    public class TicketDTO
    {
        public int Id { get; set; } 
        public int Number { get; set; }
        public int Price { get; set; }   
        public string SellerNotes { get; set; }
        public string Status { get; set; }

        //public OrderDTO Order { get; set; }
        public int EventId { get; set; }
        public EventDTO Event { get; set; }
        public int SellerId { get; set; }
        public UserDTO Seller { get; set; }

        public List<OrderDTO> Orders { get; set; }
    }

}
