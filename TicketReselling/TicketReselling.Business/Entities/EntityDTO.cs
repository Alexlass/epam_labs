﻿using System;

namespace TicketReselling.Business.Entities
{
    /// <summary>
    /// Base entity
    /// </summary>
    public abstract class EntityDTO
    {   

        /// <summary>
        /// ID
        /// </summary>
        public int Id { get; set; }
    }

}
