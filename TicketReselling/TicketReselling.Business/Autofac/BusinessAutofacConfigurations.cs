﻿using Autofac;
using TicketReselling.Business.Interfaces;
using TicketReselling.Business.Services;

namespace TicketReselling.Business.Autofac
{
    public class BusinessAutofacConfigurations : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CityService>().As<ICityService>();
            builder.RegisterType<EventService>().As<IEventService>();
            builder.RegisterType<OrderService>().As<IOrderService>();
            builder.RegisterType<TicketService>().As<ITicketService>();
            builder.RegisterType<VenueService>().As<IVenueService>();
            builder.RegisterType<UserService>().As<IUserService>();
            builder.RegisterType<RoleService>().As<IRoleService>();

            base.Load(builder);
        }
    }
}
