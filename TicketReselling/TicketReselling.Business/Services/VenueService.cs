﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TicketReselling.Business.Infrastructure;
using TicketReselling.Business.Interfaces;
using TicketReselling.DataAccess.Entities;
using TicketReselling.Business.Entities;
using TicketReselling.DataAccess.Repositories;
using System.Linq;

namespace TicketReselling.Business.Services
{
    public class VenueService : IVenueService
    {
        IRepository<Venue> repository { get; set; }

        public VenueService(IRepository<Venue> repo)
        {
            repository = repo;
        }

        public IEnumerable<VenueDTO> GetAllVenues()
        {
            var venues = repository.GetAllItems();
            if (venues.Count() == 0)
            {
                return new List<VenueDTO>();
            }
            return Mapper.Map<List<VenueDTO>>(venues);
        }

        public IEnumerable<VenueDTO> GetVenuesByCityId(int cityId)
        {
            var venues = repository.GetAllItems().Where(v => v.CityId == cityId);
            if (venues.Count() == 0)
            {
                return new List<VenueDTO>();
            }
            return Mapper.Map<List<VenueDTO>>(venues);
        }

        public VenueDTO GetVenue(int id)
        {
            if (id == null)
                throw new ValidationException("Venue id did not found", "");
            var item = repository.GetById(id);
            if (item == null)
                throw new ValidationException("Venue did not found", "");

            return Mapper.Map<VenueDTO>(item);
        }

        public void Create(string name, string address, int cityId)
        {
            var item = new VenueDTO();
            item.Name = name;
            item.Address = address;
            item.CityId = cityId;
            repository.Create(Mapper.Map<Venue>(item));
            Save();
        }

        public void Update(int id, string newName, string address, int cityId)
        {
            var venue = repository.GetById(id);
            venue.Name = newName;
            venue.Address = address;
            venue.CityId = cityId;
            repository.Update(venue);
            Save();
        }

        public void Delete(int id)
        {
            repository.Delete(id);
            Save();
        }

        private void Save()
        {
            repository.Save();
        }
    }
}
