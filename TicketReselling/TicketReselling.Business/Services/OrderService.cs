﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TicketReselling.Business.Infrastructure;
using TicketReselling.Business.Interfaces;
using TicketReselling.DataAccess.Entities;
using TicketReselling.Business.Entities;
using TicketReselling.DataAccess.Repositories;
using System.Linq;

namespace TicketReselling.Business.Services
{
    public class OrderService : IOrderService
    {
        IRepository<Order> repository { get; set; }
        IRepository<Ticket> ticketRepository { get; set; }

        public OrderService(IRepository<Order> repo, IRepository<Ticket> ticketRepository)
        {
            repository = repo;
            this.ticketRepository = ticketRepository;
        }       

        public IEnumerable<OrderDTO> GetAllOrders()
        {
            var orders = repository.GetAllItems();
            if (orders.Count() == 0)
            {
                return new List<OrderDTO>();
            }
            return Mapper.Map<List<OrderDTO>>(orders);
        }

        public OrderDTO GetOrder(int id)
        {
            if (id == null)
                throw new ValidationException("Event id did not found", "");
            var item = repository.GetById(id);
            if (item == null)
                throw new ValidationException("Event did not found", "");
            
            return Mapper.Map<OrderDTO>(item);
        }

        public IEnumerable<OrderDTO> GetUserOrders(string login)
        {
            var orders = GetAllOrders().Where(x => x.Buyer.Login == login).ToList();
            return orders;
        }

        public IEnumerable<OrderDTO> GetUserConfirmedOrders(string login)
        {
            var orders = GetAllOrders().Where(x => x.Buyer.Login == login).ToList();
            return orders.Where(x => x.Status == "Confirmed");
        }

        public IEnumerable<OrderDTO> GetUserWaitingConfirmationOrders(string login)
        {
            var orders = GetAllOrders().Where(x => x.Buyer.Login == login).ToList();
            return orders.Where(x => x.Status == "Waiting Confirmation");
        }

        public IEnumerable<OrderDTO> GetUserRejectedOrders(string login)
        {
            var orders = GetAllOrders().Where(x => x.Buyer.Login == login).ToList();
            return orders.Where(x => x.Status == "Rejected");
        }

        public void Create(int buyerId, int ticketId)
        {
            var order = new Order();
            order.Ticket = ticketRepository.GetById(ticketId);
            order.BuyerId = buyerId;
            order.Status = "Waiting Confirmation";
            order.TrackNumber = "";
            order.Comment = "";
            repository.Create(order);
            Save();
        }

        public void NotifyTheSeller(int ticketId)
        {
            var ticket = ticketRepository.GetById(ticketId);
            ticket.Status = "Waiting Confirmation";
            ticketRepository.Update(ticket);
            Save();
        }

        public void Reject(OrderDTO order)
        {
            var item = repository.GetById(order.Id);
            item.Comment = order.Comment;
            item.Status = "Rejected";
            repository.Update(item);
            Save();
        }

        public void Confirm(OrderDTO order)
        {
            var item = repository.GetById(order.Id);
            item.TrackNumber= order.Comment;
            item.Status = "Confirmed";
            repository.Update(item);
            Save();
        }

        public void Delete(int id)
        {
            repository.Delete(id);
            Save();
        }

        private void Save()
        {
            repository.Save();
        }
    }
}
