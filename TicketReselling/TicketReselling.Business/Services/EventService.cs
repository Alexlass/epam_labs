﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TicketReselling.Business.Infrastructure;
using TicketReselling.Business.Interfaces;
using TicketReselling.DataAccess.Entities;
using TicketReselling.Business.Entities;
using TicketReselling.DataAccess.Repositories;
using System.Linq;

namespace TicketReselling.Business.Services
{
    public class EventService : IEventService
    {
        private IEventRepository eventRepository { get; }
        private IMapper mapper { get; set; }

        public EventService(IEventRepository eventRepository/*, IMapper mapper*/)
        {
            this.eventRepository = eventRepository;
            //this.mapper = mapper;
        }

        public IEnumerable<EventDTO> GetAllEvents()
        {
            var events = eventRepository.GetAllItems();
            if (events.Count() == 0)
            {
                return new List<EventDTO>();
            }
            return Mapper.Map<List<EventDTO>>(events);
        }

        public EventDTO GetEvent(int id)
        {
            if (id == null)
                throw new ValidationException("Event id did not found", "");
            var item = eventRepository.GetById(id);
            if (item == null)
                throw new ValidationException("Event did not found", "");

            return Mapper.Map<EventDTO>(item);
        }

        public IEnumerable<EventDemostrationDTO> DisplayAllEvents()
        {
            var events = eventRepository.GetAllItems();
            List<EventDemostrationDTO> items = new List<EventDemostrationDTO>();

            foreach (var i in events)
            {
                EventDemostrationDTO item = new EventDemostrationDTO();                
                
                item.Id = i.Id;
                item.Banner = i.Banner;
                item.Name = i.Name;
                item.Date = i.Date;
                item.CityName = eventRepository.GetCityNameByVenueId(i.Venue.Id);
                item.VenueName = eventRepository.GetVenueNameById(i.Venue.Id);
                item.Description = i.Description;

                items.Add(item);
            }
            return items;
        }

        public void Create(string name, DateTime date, string banner, string description, int venueId)
        {
            var item = new EventDTO();
            item.Name = name;
            item.Date = date;
            item.Banner = banner;
            item.Description = description;
            item.VenueId = venueId;
            eventRepository.Create(Mapper.Map<Event>(item));
            Save();
        }

        public void Update(int id, string name, DateTime date, string banner, string description, int venueId)
        {
            var item = eventRepository.GetById(id);
            item.Name = name;
            item.Date = date;
            item.Banner = banner;
            item.Description = description;
            item.VenueId = venueId;
            eventRepository.Update(item);
            Save();
        }

        public void Update(EventDTO eventDTO)
        {
            var item = eventRepository.GetById(eventDTO.Id);
            item.Name = eventDTO.Name;
            item.Date = eventDTO.Date;
            item.Banner = eventDTO.Banner;
            item.Description = eventDTO.Description;
            item.VenueId = eventDTO.VenueId;
            eventRepository.Update(item);
            Save();
        }

        public void Delete(int id)
        {
            eventRepository.Delete(id);
            Save();
        }

        private void Save()
        {
            eventRepository.Save();
        }
    }
}
