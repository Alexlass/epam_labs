﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TicketReselling.Business.Infrastructure;
using TicketReselling.Business.Interfaces;
using TicketReselling.DataAccess.Entities;
using TicketReselling.Business.Entities;
using TicketReselling.DataAccess.Repositories;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace TicketReselling.Business.Services
{
    public class UserService : IUserService
    {
        IRepository<User> repository { get; set; }
        IRepository<Role> roleRepository { get; set; }

        public UserService(IRepository<User> userRepository, IRepository<Role> roleRepository)
        {
            repository = userRepository;
            this.roleRepository = roleRepository;
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            var users = repository.GetAllItems();
            if (users.Count() == 0)
            {
                return new List<UserDTO>();
            }
            return Mapper.Map<List<UserDTO>>(users);
        }

        public UserDTO GetUser(int id)
        {
            if (id == null)
                throw new ValidationException("User id did not found", "");
            var item = repository.GetById(id);
            if (item == null)
                throw new ValidationException("User did not found", "");
            
            return Mapper.Map<UserDTO>(item);
        }

        public string GetRoleNameByLogin (string log)
        {
            if (log == null)
                throw new ValidationException("User id did not found", "");
            var item = GetUserByLogin(log);
            if (item == null)
                throw new ValidationException("User did not found", "");
            var role = roleRepository.GetById(item.RoleId).Name;

            return role;
        }

        public UserDTO GetUserByLoginAndPassword(string log, string pass)
        {
            var users = GetAllUsers();
            var user = users.ToList().Find(x => x.Login.Equals(log) && x.Password.Equals(pass));

            return user;
        }

        public UserDTO GetUserByNormalizedLogin(string log)
        {
            var users = GetAllUsers();
            var user = users.ToList().Find(x => x.Login.ToUpper().Equals(log));

            return user;
        }

        public UserDTO GetUserByLogin(string log)
        {
            var users = GetAllUsers();
            var user = users.ToList().Find(x => x.Login.Equals(log));

            return user;
        }

        public string GetMD5Hash(string pass)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(pass);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }

            return sb.ToString();
        }

        public void Create(string login, string pass, string fname, string lname, string address, string phone, string localization, int roleId)
        {
            var item = new UserDTO();
            item.Login = login;
            item.FirstName = fname;
            item.LastName = lname;
            item.Password = GetMD5Hash(pass);
            item.RoleId = roleId;
            item.Localization = localization;
            item.PhoneNumber = phone;
            item.Address = address;
            repository.Create(Mapper.Map<User>(item));
            Save();
        }

        public void Create(string login, string pass)
        {
            var item = new UserDTO();
            item.Login = login;
            item.Password = GetMD5Hash(pass);
            item.RoleId = 2;
            item.FirstName = "";
            item.LastName = "";
            item.Localization = "en";
            item.PhoneNumber = "";
            item.Address = "";
            repository.Create(Mapper.Map<User>(item));
            Save();
        }

        public void Update(int id, string login, string pass, string fname, string lname, string address, string phone, string localization, int roleId)
        {
            var item = repository.GetById(id);
            item.Login = login;
            item.Password = GetMD5Hash(pass);
            item.RoleId = roleId;
            item.FirstName = fname;
            item.LastName = lname;
            item.Localization = localization;
            item.PhoneNumber = phone;
            item.Address = address;
            repository.Update(item);
            Save();
        }

        public void Update(UserDTO user)
        {
            var item = repository.GetById(user.Id);
            item.Login = user.Login;
            item.FirstName = user.FirstName;
            item.LastName = user.LastName;
            item.Password = GetMD5Hash(user.Password);
            item.RoleId = user.RoleId;
            item.Localization = user.Localization;
            item.PhoneNumber = user.PhoneNumber;
            item.Address = user.Address;
            repository.Update(item);
            Save();
        }

        public void Delete(int id)
        {
            repository.Delete(id);
            Save();
        }

        private void Save()
        {
            repository.Save();
        }
    }
}
