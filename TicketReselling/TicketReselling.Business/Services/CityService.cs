﻿using System;
using System.Collections.Generic;
using TicketReselling.Business.Interfaces;
using TicketReselling.DataAccess.Entities;
using TicketReselling.Business.Entities;
using AutoMapper;
using TicketReselling.Business.Infrastructure;
using TicketReselling.DataAccess.Repositories;
using System.Linq;

namespace TicketReselling.Business.Services
{
    public class CityService : ICityService
    {
        IRepository<City> repository { get; set; }

        public CityService(IRepository<City> repo)
        {
            repository = repo;
        }

        public IEnumerable<CityDTO> GetAllCities()
        {
            var cities = repository.GetAllItems();
            if (cities.Count() == 0)
            {
                return new List<CityDTO>();
            }
            return Mapper.Map<List<CityDTO>>(cities);
        }

        public CityDTO GetCity(int id)
        {
            if (id == null)
                throw new ValidationException("City id did not found", "");
            var city = repository.GetById(id);
            if (city == null)
                throw new ValidationException("City did not found", "");
            
            return Mapper.Map<CityDTO>(city);
        }

        public void Create(string name)
        {
            var city = new CityDTO();
            city.Name = name;
            repository.Create(Mapper.Map<City>(city));
            Save();            
        }

        public void Update(int id, string newName)
        {
            var city = repository.GetById(id);
            city.Name = newName;
            repository.Update(city);
            Save();
        }

        public void Delete(int id)
        {
            repository.Delete(id);
            Save();
        }

        private void Save()
        {
            repository.Save();
        }
    }
}
