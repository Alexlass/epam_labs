﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketReselling.Business.Entities;

namespace TicketReselling.Business.Services
{
    interface IEntityService<T> where T : EntityDTO
    {
        IEnumerable<T> GetAllItems();

        T GetItem(int id);
    }
}
