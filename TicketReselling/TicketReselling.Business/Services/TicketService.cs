﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TicketReselling.Business.Infrastructure;
using TicketReselling.Business.Interfaces;
using TicketReselling.DataAccess.Entities;
using TicketReselling.Business.Entities;
using TicketReselling.DataAccess.Repositories;
using System.Linq;

namespace TicketReselling.Business.Services
{
    public class TicketService : ITicketService
    {
        IRepository<Ticket> repository { get; set; }
        IRepository<User> userRepository { get; set; }
        private IEventRepository eventRepository { get; }

        public TicketService(IRepository<Ticket> repo, IEventRepository eventRepository, IRepository<User> userRepository)
        {
            repository = repo;
            this.eventRepository = eventRepository;
            this.userRepository = userRepository;
        }

        public IEnumerable<TicketDTO> GetAllTickets()
        {
            var tickets = repository.GetAllItems();
            if (tickets.Count() == 0)
            {
                return new List<TicketDTO>();
            }
            return Mapper.Map<List<TicketDTO>>(tickets);
        }

        public TicketDTO GetTicket(int id)
        {
            if (id == null)
                throw new ValidationException("Event id did not found", "");
            var item = repository.GetById(id);
            if (item == null)
                throw new ValidationException("Event did not found", "");
            
            return Mapper.Map<TicketDTO>(item);
        }

        public IEnumerable<TicketDTO> GetTicketsByEvent(int eventId)
        {
            return GetAllTickets().Where(x => x.Event.Id == eventId).ToList();
        }

        public IEnumerable<TicketDTO> GetUserTickets(string login)
        {
            var tickets = GetAllTickets().Where(x => x.Seller.Login == login).ToList();
            return tickets;
        }

        public IEnumerable<TicketDTO> GetUserSellingTickets(string login)
        {
            var tickets = GetAllTickets().Where(x => x.Seller.Login == login).ToList();
            return tickets.Where(x => x.Status == "Selling");
        }

        public IEnumerable<TicketDTO> GetUserWaitingConfirmationTickets(string login)
        {
            var tickets = GetAllTickets().Where(x => x.Seller.Login == login).ToList();
            return tickets.Where(x => x.Status == "WaitingConfirmation");
        }

        public IEnumerable<TicketDTO> GetUserSoldTickets(string login)
        {
            var tickets = GetAllTickets().Where(x => x.Seller.Login == login).ToList();
            return tickets.Where(x => x.Status == "Sold");
        }

        public void Create(int eventId, int sellerId, int price, int number)
        {
            var ticket = new Ticket();
            ticket.Event = eventRepository.GetById(eventId);
            ticket.Seller = userRepository.GetById(sellerId);
            ticket.Price = price;
            ticket.Number = number;
            ticket.Status = "Selling";
            ticket.SellerNotes = "";
            repository.Create(ticket);
            Save();
        }

        public void Reject(int id)
        {
            var item = repository.GetById(id);
            item.Status = "Selling";
            repository.Update(item);
            Save();
        }

        public void Confirm(int id)
        {
            var item = repository.GetById(id);
            item.Status = "Sold";
            repository.Update(item);
            Save();
        }

        public void Delete(int id)
        {
            repository.Delete(id);
            Save();
        }

        private void Save()
        {
            repository.Save();
        }

    }
}
