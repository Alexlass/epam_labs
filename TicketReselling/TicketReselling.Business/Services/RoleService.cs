﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TicketReselling.Business.Entities;
using TicketReselling.Business.Infrastructure;
using TicketReselling.Business.Interfaces;
using TicketReselling.DataAccess.Entities;
using TicketReselling.DataAccess.Repositories;

namespace TicketReselling.Business.Services
{
    public class RoleService : IRoleService
    {
        IRepository<Role> repository { get; set; }

        public RoleService(IRepository<Role> repo)
        {
            repository = repo;
        }

        public IEnumerable<RoleDTO> GetAllRoles()
        {
            var roles = repository.GetAllItems();
            if (roles.Count() == 0)
            {
                return new List<RoleDTO>();
            }
            return Mapper.Map<List<RoleDTO>>(roles);
        }

        public RoleDTO GetRole(int id)
        {
            if (id == null)
                throw new ValidationException("City id did not found", "");
            var role = repository.GetById(id);
            if (role == null)
                throw new ValidationException("City did not found", "");
            
            return Mapper.Map<RoleDTO>(role);
        }

        public RoleDTO GetRoleByName(string name)
        {
            var roles = GetAllRoles();
            var role = roles.ToList().Find(x => x.Name.Equals(name));

            return role;
        }

        public string GetRoleNameByUserName(string name)
        {
            var roles = GetAllRoles();
            var role = roles.ToList().Find(x => x.Name.Equals(name));

            return role.Name;
        }
    }
}
