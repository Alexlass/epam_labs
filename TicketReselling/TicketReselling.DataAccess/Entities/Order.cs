﻿using System;

namespace TicketReselling.DataAccess.Entities
{
    public class Order : Entity
    {
        public string TrackNumber { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }

        //public int TicketId { get; set; }
        public Ticket Ticket { get; set; }
        public int BuyerId { get; set; }
        public User Buyer { get; set; }        
    }

}
