﻿
using System.Collections.Generic;

namespace TicketReselling.DataAccess.Entities
{
    public class City : Entity
    {
        public string Name { get; set; }

        public List<Venue> Venues { get; set; }
    }
}