﻿using System;
using System.Collections.Generic;

namespace TicketReselling.DataAccess.Entities
{
    public class Event : Entity
    {
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public string Banner { get; set; }
        public string Description { get; set; }

        public int VenueId { get; set; }
        public Venue Venue { get; set; }
        public List<Ticket> Tickets { get; set; }
    }

}
