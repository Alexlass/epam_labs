﻿using System.Collections.Generic;
namespace TicketReselling.DataAccess.Entities
{
    public class Ticket : Entity
    { 
        public int Number { get; set; }
        public int Price { get; set; }
        public string SellerNotes { get; set; }
        public string Status { get; set; }

        public int SellerId { get; set; }
        public User Seller { get; set; }
        public int EventId { get; set; }
        public Event Event { get; set; }
    }

}
