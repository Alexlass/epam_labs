﻿using System;
using System.Collections.Generic;

namespace TicketReselling.DataAccess.Entities
{
    public class Venue : Entity
    {
        public string Name { get; set; }
        public string Address { get; set; }

        public int CityId { get; set; } 
        public City City { get; set; }
        public List<Event> Events { get; set; }
    }

}
