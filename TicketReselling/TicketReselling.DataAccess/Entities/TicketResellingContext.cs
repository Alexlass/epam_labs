﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations.Schema;

namespace TicketReselling.DataAccess.Entities
{
    public class TicketResellingContext : DbContext
    {
        public TicketResellingContext(DbContextOptions<TicketResellingContext> options) : base(options)
        {
        }

        public DbSet<City> Cities { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Venue> Venues { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>().ToTable("City");
            modelBuilder.Entity<Event>().ToTable("Event");
            modelBuilder.Entity<Order>().ToTable("Order");
            modelBuilder.Entity<Role>().ToTable("Role");
            modelBuilder.Entity<User>().ToTable("User");
            modelBuilder.Entity<Ticket>().ToTable("Ticket");
            modelBuilder.Entity<Venue>().ToTable("Venue");

            modelBuilder.Entity<Order>()
                .HasOne(o => o.Ticket)
                .WithMany();

            modelBuilder.Entity<Ticket>()
                .HasOne(p => p.Seller)
                .WithMany(b => b.Tickets)
                .HasForeignKey(b => b.SellerId);

            modelBuilder.Entity<Ticket>()
                .HasOne(p => p.Event)
                .WithMany(i => i.Tickets)
                .HasForeignKey(b => b.EventId);

            modelBuilder.Entity<Venue>()
                .HasOne(p => p.City)
                .WithMany(i => i.Venues)
                .HasForeignKey(b => b.CityId);

            modelBuilder.Entity<Event>()
                .HasOne(p => p.Venue)
                .WithMany(i => i.Events)
                .HasForeignKey(b => b.VenueId);

            modelBuilder.Entity<Order>()
                .HasOne(p => p.Buyer)
                .WithMany(i => i.Orders)
                .HasForeignKey(b => b.BuyerId);

            modelBuilder.Entity<User>()
                .HasOne(p => p.Role)
                .WithMany(i => i.Users)
                .HasForeignKey(b => b.RoleId);
        }
    }
}
