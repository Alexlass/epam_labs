﻿
using System.Collections.Generic;

namespace TicketReselling.DataAccess.Entities
{
    public class User : Entity
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Localization { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        public Role Role { get; set; }
        public int RoleId { get; set; }
        public List<Ticket> Tickets { get; set; }
        public List<Order> Orders { get; set; }
    }

}
