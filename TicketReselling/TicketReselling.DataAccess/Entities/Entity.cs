﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TicketReselling.DataAccess.Entities
{
    /// <summary>
    /// Base entity
    /// </summary>
    public abstract class Entity
    {

        /// <summary>
        /// ID
        /// </summary>
       // [DatabaseGeneratedAttribute(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
    }

}
