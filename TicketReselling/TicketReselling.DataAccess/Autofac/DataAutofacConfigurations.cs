﻿using Autofac;
using TicketReselling.DataAccess.Entities;
using TicketReselling.DataAccess.Repositories;

namespace TicketReselling.DataAccess.Autofac
{
    public class DataAutofacConfigurations : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<CityRepository>().As<IRepository<City>>();
           // builder.RegisterType<EventRepository>().As<IRepository<Event>>();
            builder.RegisterType<EventRepository>().As<IEventRepository>();
            builder.RegisterType<TicketRepository>().As<IRepository<Ticket>>();
            builder.RegisterType<OrderRepository>().As<IRepository<Order>>();
            builder.RegisterType<UserRepository>().As<IRepository<User>>();
            builder.RegisterType<VenueRepository>().As<IRepository<Venue>>();
            builder.RegisterType<RoleRepository>().As<IRepository<Role>>();

            base.Load(builder);
        }
    }
}
