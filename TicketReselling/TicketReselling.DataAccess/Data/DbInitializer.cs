﻿using System;
using System.Linq;
using TicketReselling.DataAccess.Entities;
using System.Security.Cryptography;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace TicketReselling.DataAccess.Data
{
    public static class DbInitializer
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<TicketResellingContext>();            

            var roles = new Role[]
           {
                new Role {Name = "Admin"},
                new Role {Name = "User"}
           };

            if (!context.Roles.Any())
            {
                context.Roles.AddRange(roles);
            }
            
            context.SaveChanges();

            var users = new User[]
            {
                new User {RoleId = 1, Role = roles[0], Address = "Brest, Pobeda street, 24", FirstName = "Admin", LastName = "Admin", Localization = "en",Login = "Admin", Password = GetMD5Hash("Admin"), PhoneNumber = "80259632145" },
                new User {RoleId = 2, Role = roles[1], Address = "Minsk, Kirova street, 8-69", FirstName = "Mihail", LastName = "Vasilec", Localization = "be", Login = "CoolChilyPepper", Password = GetMD5Hash("qwerty"), PhoneNumber = "80336985263" },
                new User {RoleId = 2, Role = roles[1], Address = "Moscow, Vokzalnaya avenue, 4-56", FirstName = "Petya", LastName = "Ivanov", Localization = "ru", Login = "Ventura", Password = GetMD5Hash("kitty"), PhoneNumber = "7895264442" },
                new User {RoleId = 2, Role = roles[1], Address = "Gomel, Chapaeva street, 7-25", FirstName = "Anna", LastName = "Kovalenko", Localization = "ru", Login = "Ann", Password = GetMD5Hash("12345"), PhoneNumber = "80251234569" }

            };

            if (!context.Users.Any())
            {
                context.Users.AddRange(users);
            }
            context.SaveChanges();

            var cities = new City[]
            {
                new City {Name = "Gomel"},
                new City {Name = "Kiev"},
                new City {Name = "Moscow"},
                new City {Name = "St. Peterburg"},
                new City {Name = "Minsk"},
                new City {Name = "Mogilev"}
            };

            if (!context.Cities.Any())
            {
                context.Cities.AddRange(cities);
            }
            context.SaveChanges();

            var venues = new Venue[]
            {
                new Venue {Name = "Grand Theatre", Address = "Theatre Square, 1", CityId = 1, City = cities[0]},
                new Venue {Name = "Sports Complex Olympic", Address = "Olympic avenue, 16", CityId = 1, City = cities[0]},
                new Venue {Name = "Ice Palace", Address = "Five-year plans avenue, 1", CityId = 2, City = cities[1]},
                new Venue {Name = "Grand Drama Theater", Address = "Theatre Square, 1", CityId = 2, City = cities[1]},
                new Venue {Name = "Minsk-arena", Address = "Winners Prospect, 111", CityId = 5, City = cities[4]},
                new Venue {Name = "Palace of Sports", Address = "Lenin Square, 1", CityId = 1, City = cities[0]},
                new Venue {Name = "Gomel Regional Drama Theatre", Address = "The International Convention Centre, Broad St.", CityId = 4, City = cities[3]},
                new Venue {Name = "Cinema named after Kalinin", Address = "Kommunarov Street, 4", CityId = 4, City = cities[3]},
                new Venue {Name = "Club Atlas", Address = "Sechevih strelcov street, 37-41", CityId = 5, City = cities[4]},
                new Venue {Name = "StereoPlaza", Address = "V. Lobanovskogo avenue, 119", CityId = 5, City = cities[4]},
                new Venue {Name = "Ice Palace", Address = "Theatre Square, 82", CityId = 6, City = cities[5]},
                new Venue {Name = "Sports Complex", Address = "Winners Prospect, 25", CityId = 6, City = cities[5]}

            };
            
            if (!context.Venues.Any())
            {
                context.Venues.AddRange(venues);
            }
            context.SaveChanges();

            var events = new Event[]
            {
                new Event { Banner = "\\images\\EventImages\\Korn.jpg", Date = new DateTime(2017,2,3), Description = "KORN will come in your town!", Name = "KORN", VenueId = 1, Venue = venues[0]},
                new Event { Banner = "\\images\\EventImages\\navi.jpg", Date = new DateTime(2017,2,13), Description = "The most romantic scenes of the Belarusian duo invites all lovers to music lovers in the emotions, in love with each other - a large presentation of a new long-awaited album.", Name = "Navi", VenueId = 2, Venue = venues[1]},
                new Event { Banner = "\\images\\EventImages\\Nusha.jpg", Date = new DateTime(2017,2,23), Description = "Nyusha - Tsunami. Large Olympic concert.", Name = "Nusha", VenueId = 3, Venue = venues[2]},
                new Event {Banner = "\\images\\EventImages\\Rammstein.jpg", Date = new DateTime(2017,3,14), Description = "Hooray, after a rather long break, the Germans were again on the road and so close to us. Do not miss the piercing arsenal of hits under the roar of fireworks and avant-garde stage antics.", Name = "Rammstein", VenueId = 4, Venue = venues[3]},
                new Event {Banner = "\\images\\EventImages\\GreenDay.jpg", Date = new DateTime(2017,4,23), Description = "Finally, the Russians will be able to buy tickets for Green Day and see a live performance of the famous band.", Name = "Green Day", VenueId = 5, Venue = venues[4]},
                new Event {Banner = "\\images\\EventImages\\onerepublic.jpg", Date = new DateTime(2017,5,16), Description = "OneRepublic - a unique mixture of rock rhythms with elements of electronic music, gospel, blues and folk music performed by the most extraordinary musicians of today.", Name = "One republic", VenueId = 6, Venue = venues[5]},
                new Event {Banner = "\\images\\EventImages\\Disturbed.jpg", Date = new DateTime(2017,6,16), Description = "Disturbed returned to Russia after 13 years with the big solo concerts in Moscow and St. Petersburg and the presentation of the album «Immortalized».", Name = "Disturbed", VenueId = 7, Venue = venues[6]},
                new Event {Banner = "\\images\\EventImages\\bi2.jpg", Date = new DateTime(2017,5,26), Description = "An impressive is not only the musical component. The program promises to surprise and a new set design, and unique light show.", Name = "BI-2", VenueId = 8, Venue = venues[7]},
                new Event {Banner = "\\images\\EventImages\\Imagine-Dragons.jpg", Date = new DateTime(2017,7,28), Description = "They - the sensation of recent years. For four years, the band came out two albums that have garnered the most popular in the world music awards, including a Grammy.", Name = "Imagine Dragons", VenueId = 9, Venue = venues[8]},
                new Event {Banner = "\\images\\EventImages\\Comedy_Club.jpg", Date = new DateTime(2017,8,21), Description = "They are not the most original names, but the jokes do not occupy acute.", Name = "Comedy Club", VenueId = 10, Venue = venues[9]},
                new Event {Banner = "\\images\\EventImages\\Avril_Lavinge.jpg", Date = new DateTime(2017,9,16), Description = "Canadian singer, songwriter, actress and designer.", Name = "Avril Lavigne", VenueId = 11, Venue = venues[10]},
                new Event {Banner = "\\images\\EventImages\\kirkorov.jpg", Date = new DateTime(2017,10,4), Description = "King of the Russian pop scene Philip will present a new show, I, the producer of which will be Franco Dragone, known for his work for the Cirque du Solly and Celine Dion.", Name = "Philip Kirkorov", VenueId = 12, Venue = venues[11]}
            };

            if (!context.Events.Any())
            {
                context.Events.AddRange(events);
            }
            context.SaveChanges();

            var tickets = new Ticket[]
            {
                new Ticket {Number = 1, Price = 100, EventId = 1, Event = events[0], SellerId = 3, Seller = users[2], Status= "Selling", SellerNotes = "mail only" },
                new Ticket {Number = 2, Price = 40, EventId = 1, Event = events[0], SellerId = 4, Seller = users[3], Status= "Selling", SellerNotes = "payment by Webmoney"},
                new Ticket {Number = 3, Price = 55, EventId = 2, Event = events[1], SellerId = 2, Seller = users[1], Status= "Selling", SellerNotes = "negotiated price"  },
                new Ticket {Number = 1, Price = 65, EventId = 2, Event = events[1], SellerId = 3, Seller = users[2], Status= "Selling", SellerNotes = "possible bargain" },
                new Ticket {Number = 2, Price = 95, EventId = 3, Event = events[2], SellerId = 4, Seller = users[3], Status= "Selling", SellerNotes = "Cash only" },
                new Ticket {Number = 3, Price = 130, EventId = 4, Event = events[3], SellerId = 2, Seller = users[1],Status= "Selling", SellerNotes = "payment by Webmoney" },
                new Ticket {Number = 1, Price = 60, EventId = 5, Event = events[4], SellerId = 4, Seller = users[3],Status= "Selling", SellerNotes = "post for your money" },
                new Ticket {Number = 2, Price = 55, EventId = 6, Event = events[5], SellerId = 3, Seller = users[2],Status= "Selling", SellerNotes = "Cash only" },
                new Ticket {Number = 3, Price = 100, EventId = 7, Event = events[6], SellerId = 3, Seller = users[2],Status= "Selling", SellerNotes = "payment by Webmoney" },
                new Ticket {Number = 1, Price = 70, EventId = 7, Event = events[6], SellerId = 3, Seller = users[2],Status= "Selling", SellerNotes = "negotiated price" },
                new Ticket {Number = 2, Price = 45, EventId = 8, Event = events[7], SellerId = 2, Seller = users[1],Status= "Selling", SellerNotes = "payment by Webmoney" },
                new Ticket {Number = 3, Price = 65, EventId = 8, Event = events[7], SellerId = 4, Seller = users[3],Status= "Selling", SellerNotes = "for your money" },
                new Ticket {Number = 4, Price = 23, EventId = 9, Event = events[8], SellerId = 3, Seller = users[2],Status= "Selling", SellerNotes = "Cash only" },
                new Ticket {Number = 1, Price = 30, EventId = 9, Event = events[8], SellerId = 4, Seller = users[3],Status= "Selling", SellerNotes = "payment by Webmoney" },
                new Ticket {Number = 2, Price = 120, EventId = 10, Event = events[9], SellerId = 2, Seller = users[1],Status= "Selling", SellerNotes = "payment by Webmoney" },
                new Ticket {Number = 3, Price = 23, EventId = 10, Event = events[9], SellerId = 3, Seller = users[2],Status= "Selling", SellerNotes = "payment by Webmoney" },
                new Ticket {Number = 1, Price = 78, EventId = 11, Event = events[10], SellerId = 4, Seller = users[3],Status= "Selling", SellerNotes = "Cash only" },
                new Ticket {Number = 2, Price = 72, EventId = 11, Event = events[10], SellerId = 3, Seller = users[2], Status= "Selling", SellerNotes = "negotiated price" },
                new Ticket {Number = 3, Price = 158, EventId = 12, Event = events[11], SellerId = 2, Seller = users[1], Status= "Selling", SellerNotes = "payment by Webmoney" },
                new Ticket {Number = 1, Price = 72, EventId = 12, Event = events[11], SellerId = 3, Seller = users[2], Status= "Selling", SellerNotes = "possible bargain" }
            };

            if (!context.Tickets.Any())
            {
                context.Tickets.AddRange(tickets);
            }
            context.SaveChanges();

            var orders = new Order[]
            {
                //new Order {Status = "Waiting Confirmation", TrackNumber = "BS458224582", Ticket = tickets[0], BuyerId = 2, Buyer = users[1]},
                //new Order {Status = "Waiting Confirmation", TrackNumber = "MS434258791", Ticket = tickets[1], BuyerId = 3, Buyer = users[2] },
                //new Order {Status = "Waiting Confirmation", TrackNumber = "RK894125475", Ticket = tickets[2], BuyerId = 4, Buyer = users[3]},
                //new Order {Status = "Waiting Confirmation", TrackNumber = "NE3RT434786", Ticket = tickets[3], BuyerId = 2, Buyer = users[1]},
                //new Order {Status = "Rejected", TrackNumber = "MS434258581",Ticket = tickets[4], BuyerId = 3, Buyer = users[2]},
                //new Order {Status = "Confirmed", TrackNumber = "E2238520482", Ticket = tickets[5], BuyerId = 4, Buyer = users[3]},
                //new Order {Status = "Confirmed", TrackNumber = "AE111111111", Ticket = tickets[6],  BuyerId = 3, Buyer = users[2]},
                //new Order {Status = "Waiting Confirmation", TrackNumber = "14785236999", Ticket = tickets[7], BuyerId = 2, Buyer = users[1]},
                //new Order {Status = "Waiting Confirmation", TrackNumber = "RF225896303", Ticket = tickets[8], BuyerId = 4, Buyer = users[3]},
                //new Order {Status = "Confirmed", TrackNumber = "BS458224558", Ticket = tickets[9], BuyerId = 2, Buyer = users[1]}
            };
            if (!context.Orders.Any())
            {
                context.Orders.AddRange(orders);
            }
            context.SaveChanges();
        }


        public static string GetMD5Hash(string pass)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(pass);
            byte[] hash = md5.ComputeHash(inputBytes);
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }
    }
}