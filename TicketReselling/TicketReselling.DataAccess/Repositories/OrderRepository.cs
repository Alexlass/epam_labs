﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketReselling.DataAccess.Entities;

namespace TicketReselling.DataAccess.Repositories
{
    public class OrderRepository : IRepository<Order>
    {
        TicketResellingContext context;

        public OrderRepository(TicketResellingContext context)
        {
            this.context = context;
        }        

        public IEnumerable<Order> GetAllItems()
        {
            return context.Orders.Include(o => o.Ticket).ThenInclude(t => t.Seller).Include(o => o.Ticket).ThenInclude(t => t.Event).ThenInclude(e => e.Venue).ThenInclude(v => v.City).Include(t => t.Buyer).ThenInclude(u => u.Role).ToList();
        }

        public Order GetById(int id)
        {            
            return context.Orders.Include(o => o.Ticket).ThenInclude(t => t.Seller).Include(o => o.Ticket).ThenInclude(t => t.Event).ThenInclude(e => e.Venue).ThenInclude(v => v.City).Include(t => t.Buyer).ThenInclude(u => u.Role).FirstOrDefault(o => o.Id == id);
        }

        public void Create(Order item)
        {
            context.Orders.Add(item);
        }

        public void Update(Order item)
        {
            context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Order item = context.Orders.FirstOrDefault(o => o.Id == id);
            if (item != null)
                context.Orders.Remove(item);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
