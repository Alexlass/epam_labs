﻿using System;
using System.Collections.Generic;
using System.Linq;
using TicketReselling.DataAccess.Entities;

namespace TicketReselling.DataAccess.Repositories
{
    public class RoleRepository : IRepository<Role>
    {
        TicketResellingContext context;

        public RoleRepository(TicketResellingContext context)
        {
            this.context = context;
        }

        public void Create(Role item)
        {
            throw new NotImplementedException();
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Role> GetAllItems()
        {
            return context.Roles;
        }

        public Role GetById(int id)
        {
            return context.Roles.FirstOrDefault(e => e.Id == id);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Update(Role item)
        {
            throw new NotImplementedException();
        }
    }
}
