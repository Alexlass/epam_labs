﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TicketReselling.DataAccess.Entities;

namespace TicketReselling.DataAccess.Repositories
{
    public class VenueRepository : IRepository<Venue>
    {
        TicketResellingContext context;

        public VenueRepository(TicketResellingContext context)
        {
            this.context = context;
        }
        
        public IEnumerable<Venue> GetAllItems()
        {
            return context.Venues.Include(v => v.City).Include(v => v.Events).ToList();
        }

        public Venue GetById(int id)
        {
            return context.Venues.Include(v => v.City).Include(v => v.Events).FirstOrDefault(e => e.Id == id);
        }

        public void Create(Venue item)
        {
            context.Venues.Add(item);
        }

        public void Update(Venue item)
        {
            context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Venue item = context.Venues.FirstOrDefault(v => v.Id == id);
            if (item != null)
                context.Venues.Remove(item);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
