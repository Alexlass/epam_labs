﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TicketReselling.DataAccess.Entities;

namespace TicketReselling.DataAccess.Repositories
{
    public class CityRepository : IRepository<City>
    {
        TicketResellingContext context;        

        public CityRepository(TicketResellingContext context)
        {
            this.context = context;
        }
               
        public IEnumerable<City> GetAllItems()
        {
            return context.Cities.ToList();
        }

        public City GetById(int id)
        {
            return context.Cities.FirstOrDefault(e => e.Id == id);
        }

        public void Create(City item)
        {
            context.Cities.Add(item);
        }

        public void Update(City item)
        {
            context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            City item = context.Cities.FirstOrDefault(c => c.Id == id);
            if (item != null)
                context.Cities.Remove(item);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
