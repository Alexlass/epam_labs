﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TicketReselling.DataAccess.Entities;

namespace TicketReselling.DataAccess.Repositories
{
    public class EventRepository : IEventRepository
    {
        TicketResellingContext context;

        public EventRepository(TicketResellingContext context)
        {
            this.context = context;
        }

        public IEnumerable<Event> GetAllItems()
        {
            return context.Events.Include(e => e.Venue).ThenInclude(v => v.City).ToList();
        }

        public Event GetById(int id)
        {
            return context.Events.Include(e => e.Venue).ThenInclude(v => v.City).FirstOrDefault(e => e.Id == id);
        }

        public void Create(Event item)
        {
            context.Events.Add(item);
        }

        public void Update(Event item)
        {
            context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            Event item = context.Events.FirstOrDefault(e => e.Id == id);
            if (item != null)
                context.Events.Remove(item);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public string GetVenueNameById(int id)
        {
            return context.Events.Include(e => e.Venue).ToList().Find(e => e.Id.Equals(id)).Venue.Name;
        }

        public string GetCityNameByVenueId(int id)
        {
            int cityId = context.Venues.ToList().Find(x => x.Id.Equals(id)).City.Id;
            return context.Events.ToList().Find(x => x.Id.Equals(cityId)).Name;
        }
    }
}
