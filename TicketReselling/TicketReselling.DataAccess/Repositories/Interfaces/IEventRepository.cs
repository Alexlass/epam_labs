﻿using System.Collections.Generic;
using TicketReselling.DataAccess.Entities;

namespace TicketReselling.DataAccess.Repositories
{
    public interface IEventRepository
    {
        IEnumerable<Event> GetAllItems();

        Event GetById(int id);

        string GetVenueNameById(int id);

        string GetCityNameByVenueId(int id);

        void Create(Event item);

        void Update(Event item);

        void Delete(int id);

        void Save();
    }
}
