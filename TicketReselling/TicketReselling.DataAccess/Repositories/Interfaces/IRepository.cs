﻿using System;
using System.Collections.Generic;
using TicketReselling.DataAccess.Entities;

namespace TicketReselling.DataAccess.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        IEnumerable<T> GetAllItems();

        T GetById(int id);

        void Create(T item);

        void Update(T item);

        void Delete(int id);

        void Save();
    }
}
