﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TicketReselling.DataAccess.Entities;

namespace TicketReselling.DataAccess.Repositories
{
    public class TicketRepository : IRepository<Ticket>
    {
        TicketResellingContext context;

        public TicketRepository(TicketResellingContext context)
        {
            this.context = context;
        }

        public IEnumerable<Ticket> GetAllItems()
        {
            return context.Tickets.Include(t => t.Seller).ThenInclude(e => e.Role).Include(t => t.Event).ThenInclude(e => e.Venue).ThenInclude(v => v.City).ToList();
        }

        public Ticket GetById(int id)
        {
            return context.Tickets.Include(t => t.Seller).ThenInclude(e => e.Role).Include(t => t.Event).ThenInclude(e => e.Venue).ThenInclude(v => v.City).FirstOrDefault(e => e.Id == id);
        }

        public IEnumerable<Ticket> GetTicketsByEvent(int eventId)
        {
            return GetAllItems().Where(t => t.Event.Id == eventId).ToList(); 
        }

        public void Create(Ticket item)
        {
            context.Tickets.Add(item);
        }

        public void Delete(int id)
        {
            Ticket item = context.Tickets.FirstOrDefault(t => t.Id == id);
            if (item != null)
                context.Tickets.Remove(item);
        }

        public void Save()
        {
            context.SaveChanges();
        }

        public void Update(Ticket item)
        {
            context.Entry(item).State = EntityState.Modified;
        }
    }
}
