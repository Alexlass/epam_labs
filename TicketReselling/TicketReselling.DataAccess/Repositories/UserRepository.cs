﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using TicketReselling.DataAccess.Entities;

namespace TicketReselling.DataAccess.Repositories
{
    public class UserRepository : IRepository<User>
    {
        TicketResellingContext context;

        public UserRepository(TicketResellingContext context)
        {
            this.context = context;
        }

        public IEnumerable<User> GetAllItems()
        {
            return context.Users.Include(u => u.Role).Include(u => u.Tickets).Include(u => u.Orders).ToList();
        }

        public User GetById(int id)
        {
            return context.Users.Include(u => u.Role).FirstOrDefault(e => e.Id == id);
        }

        public void Create(User item)
        {
            context.Users.Add(item);
        }

        public void Update(User item)
        {
            context.Entry(item).State = EntityState.Modified;
        }

        public void Delete(int id)
        {
            User item = context.Users.FirstOrDefault(u => u.Id == id);
            if (item != null)
                context.Users.Remove(item);
        }

        public void Save()
        {
            context.SaveChanges();
        }
    }
}
